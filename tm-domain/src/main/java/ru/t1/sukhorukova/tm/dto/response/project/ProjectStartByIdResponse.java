package ru.t1.sukhorukova.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectStartByIdResponse extends AbstractProjectResponse {

    public ProjectStartByIdResponse(@Nullable ProjectDTO project) {
        super(project);
    }

}
