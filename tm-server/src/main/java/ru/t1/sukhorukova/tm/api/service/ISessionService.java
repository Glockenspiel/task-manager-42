package ru.t1.sukhorukova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.model.SessionDTO;

import java.util.Collection;
import java.util.List;

public interface ISessionService {

    @NotNull
    SessionDTO add(@Nullable SessionDTO session);

    @Nullable
    SessionDTO add(
            @Nullable String userId,
            @Nullable SessionDTO session
    );

    @NotNull
    Collection<SessionDTO> add(@NotNull Collection<SessionDTO> sessions);

    @NotNull
    Collection<SessionDTO> set(@NotNull Collection<SessionDTO> sessions);

    @NotNull
    List<SessionDTO> findAll();

    @NotNull
    List<SessionDTO> findAll(@Nullable String userId);

    @Nullable
    SessionDTO findOneById(@Nullable String id);

    @Nullable
    SessionDTO findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    SessionDTO findOneByIndex(@Nullable Integer index);

    @Nullable
    SessionDTO findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @NotNull
    SessionDTO removeOne(@Nullable SessionDTO session);

    SessionDTO removeOne(
            @Nullable String userId,
            @Nullable SessionDTO session
    );

    @Nullable
    SessionDTO removeOneById(@Nullable String id);

    @Nullable
    SessionDTO removeOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    SessionDTO removeOneByIndex(@Nullable Integer index);

    @Nullable
    SessionDTO removeOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    void removeAll();

    void removeAll(@Nullable String userId);

    int getSize();

    int getSize(@Nullable String userId);

    boolean existsById(String id);

    boolean existsById(
            @Nullable String userId,
            @Nullable String id
    );

}
