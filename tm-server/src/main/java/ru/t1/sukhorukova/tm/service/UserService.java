package ru.t1.sukhorukova.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.api.repository.IProjectRepository;
import ru.t1.sukhorukova.tm.api.repository.ISessionRepository;
import ru.t1.sukhorukova.tm.api.repository.ITaskRepository;
import ru.t1.sukhorukova.tm.api.repository.IUserRepository;
import ru.t1.sukhorukova.tm.api.service.IConnectionService;
import ru.t1.sukhorukova.tm.api.service.IPropertyService;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.exception.entity.EntityNotFoundException;
import ru.t1.sukhorukova.tm.exception.entity.UserNotFoundException;
import ru.t1.sukhorukova.tm.exception.entity.ValueIsNullException;
import ru.t1.sukhorukova.tm.exception.field.*;
import ru.t1.sukhorukova.tm.exception.user.ExistsEmailException;
import ru.t1.sukhorukova.tm.exception.user.ExistsLoginException;
import ru.t1.sukhorukova.tm.exception.user.RoleEmptyException;
import ru.t1.sukhorukova.tm.dto.model.UserDTO;
import ru.t1.sukhorukova.tm.util.HashUtil;
import ru.t1.sukhorukova.tm.api.service.IUserService;

import java.util.Collection;
import java.util.List;

public class UserService implements IUserService {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IPropertyService propertyService
    ) {
        this.connectionService = connectionService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public UserDTO add(@Nullable final UserDTO user) {
        if (user == null) throw new EntityNotFoundException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.add(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

        return user;
    }

    @NotNull
    @Override
    public Collection<UserDTO> add(@Nullable final Collection<UserDTO> users) {
        if (users == null) throw new ValueIsNullException();

        for (final UserDTO user : users) {
            add(user);
        }

        return users;
    }

    @NotNull
    @Override
    public Collection<UserDTO> set(@Nullable final Collection<UserDTO> users) {
        if (users == null) throw new ValueIsNullException();

        removeAll();

        return add(users);
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);

        try {
            return repository.findAll();
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public UserDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            return repository.findOneById(id);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public UserDTO findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @NotNull final List<UserDTO> users = findAll();
        return users.get(index);
    }

    @Override
    public void removeAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            sessionRepository.removeAll();
            taskRepository.removeAll();
            projectRepository.removeAll();
            userRepository.removeAll();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public UserDTO removeOne(@Nullable final UserDTO user) {
        if (user == null) return null;
        @NotNull final String userId = user.getId();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            sessionRepository.removeAllByUserId(userId);
            taskRepository.removeAllByUserId(userId);
            projectRepository.removeAllByUserId(userId);
            userRepository.removeOneById(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

        return user;
    }

    @Nullable
    @Override
    public UserDTO removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable UserDTO user = findOneById(id);
        if (user == null) return null;

        return removeOne(user);
    }

    @Nullable
    @Override
    public UserDTO removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @NotNull final List<UserDTO> users = findAll();
        return removeOneById(users.get(index).getId());
    }

    @Override
    public int getSize() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            return repository.getSize();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return findOneById(id) != null;
    }

    /////////////////////////////////////////////////////////////////////////////////

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();

        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return add(user);
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();

        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        return add(user);
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();

        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        return add(user);
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            return repository.findByLogin(login);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            return repository.findByEmail(email);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public UserDTO removeOneByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();

        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new EntityNotFoundException();
        return removeOne(user);
    }

    @Nullable
    @Override
    public UserDTO removeOneByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();

        @Nullable final UserDTO user = findByEmail(email);
        if (user == null) throw new EntityNotFoundException();
        return removeOne(user);
    }

    @NotNull
    @Override
    public UserDTO setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();

        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new EntityNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.setPasswordHashById(user.getId(), user.getPasswordHash());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

        return user;
    }

    @NotNull
    @Override
    public UserDTO updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new EntityNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.update(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

        return user;
    }

    @NotNull
    private UserDTO setLockOneByLogin(
            @Nullable final String login,
            @NotNull final Boolean locked
    ) {
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(locked);

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.setLockById(user.getId(), locked);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

        return user;
    }

    @NotNull
    @Override
    public UserDTO lockOneByLogin(@Nullable final String login) {
        return setLockOneByLogin(login, true);
    }

    @NotNull
    @Override
    public UserDTO unlockOneByLogin(@Nullable final String login) {
        return setLockOneByLogin(login, false);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

}
