package ru.t1.sukhorukova.tm.service;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.api.repository.IProjectRepository;
import ru.t1.sukhorukova.tm.api.repository.ISessionRepository;
import ru.t1.sukhorukova.tm.api.repository.ITaskRepository;
import ru.t1.sukhorukova.tm.api.repository.IUserRepository;
import ru.t1.sukhorukova.tm.api.service.IConnectionService;
import ru.t1.sukhorukova.tm.api.service.IDatabaseProperty;
import ru.t1.sukhorukova.tm.dto.model.ProjectDTO;
import ru.t1.sukhorukova.tm.dto.model.SessionDTO;
import ru.t1.sukhorukova.tm.dto.model.TaskDTO;
import ru.t1.sukhorukova.tm.dto.model.UserDTO;
import ru.t1.sukhorukova.tm.model.Project;
import ru.t1.sukhorukova.tm.model.Session;
import ru.t1.sukhorukova.tm.model.Task;
import ru.t1.sukhorukova.tm.model.User;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IDatabaseProperty databaseProperty;

    @NotNull
    private final SqlSessionFactory sqlSessionFactory;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull final IDatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
        sqlSessionFactory = getSqlSessionFactory();
        entityManagerFactory = factory();
    }

    @NotNull
    @Override
    public SqlSession getSqlConnection() {
        return sqlSessionFactory.openSession();
    }

    @NotNull
    private SqlSessionFactory getSqlSessionFactory() {
        @NotNull final String driver = databaseProperty.getDatabaseDriver();
        @NotNull final String username = databaseProperty.getDatabaseLogin();
        @NotNull final String password = databaseProperty.getDatabasePassword();
        @NotNull final String url = databaseProperty.getDatabaseUrl();

        @NotNull final DataSource dataSource = new PooledDataSource(driver,  url,username, password);

        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("development", transactionFactory,dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

    @NotNull
    @Override
    public EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, databaseProperty.getDatabaseDriver());
        settings.put(org.hibernate.cfg.Environment.URL, databaseProperty.getDatabaseUrl());
        settings.put(org.hibernate.cfg.Environment.USER, databaseProperty.getDatabaseLogin());
        settings.put(org.hibernate.cfg.Environment.PASS, databaseProperty.getDatabasePassword());
        settings.put(org.hibernate.cfg.Environment.DIALECT, databaseProperty.getDatabaseDialect());
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, databaseProperty.getDatabaseHBM2DDLAuto());
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, databaseProperty.getDatabaseShowSQL());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}
