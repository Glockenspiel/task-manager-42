package ru.t1.sukhorukova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectRepository {

    @Insert(
            "INSERT INTO TM_PROJECT (ID, NAME, DESCRIPTION, USER_ID, STATUS, CREATED) " +
            "VALUES (#{id}, #{name}, #{description}, #{userId}, #{status}, #{created})"
    )
    void add(@NotNull final ProjectDTO project);

    @Select("SELECT * FROM TM_PROJECT")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable List<ProjectDTO> findAll();

    @Select("SELECT * FROM TM_PROJECT WHERE USER_ID = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable List<ProjectDTO> findAllByUserId(@NotNull final String userId);

    @Select("SELECT * FROM TM_PROJECT WHERE USER_ID = #{userId} ORDER BY NAME")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable List<ProjectDTO> findAllSortByName(@NotNull final String userId);

    @Select("SELECT * FROM TM_PROJECT WHERE USER_ID = #{userId} ORDER BY STATUS")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable List<ProjectDTO> findAllSortByStatus(@NotNull final String userId);

    @Select("SELECT * FROM TM_PROJECT WHERE USER_ID = #{userId} ORDER BY DESCRIPTION")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable List<ProjectDTO> findAllSortByCreated(@NotNull final String userId);

    @Select("SELECT * FROM TM_PROJECT WHERE ID = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable ProjectDTO findOneById(@NotNull final String id);

    @Select("SELECT * FROM TM_PROJECT WHERE ID = #{id} AND USER_ID = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable ProjectDTO findOneByIdByUserId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("id") final String id
    );

    @Delete("DELETE FROM TM_PROJECT WHERE ID = #{id}")
    void removeOneById(@NotNull final String id);

    @Delete("DELETE FROM TM_PROJECT WHERE ID = #{id} AND USER_ID = #{userId}")
    void removeOneByIdByUserId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("id") final String id
    );

    @Delete("DELETE FROM TM_PROJECT")
    void removeAll();

    @Delete("DELETE FROM TM_PROJECT WHERE USER_ID = #{userId}")
    void removeAllByUserId(@NotNull final String userId);

    @Select("SELECT COUNT(1) FROM TM_PROJECT")
    int getSize();

    @Select("SELECT COUNT(1) FROM TM_PROJECT WHERE USER_ID = #{userId}")
    int getSizeByUserId(@NotNull final String userId);

    @Update(
            "UPDATE TM_PROJECT" +
            "   SET NAME = #{name}," +
            "       DESCRIPTION = #{description}" +
            " WHERE ID = #{id} AND USER_ID = #{userId}"
    )
    void updateById(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("id") final String id,
            @NotNull @Param("name") final String name,
            @NotNull @Param("description") final String description
    );

    @Update(
            "UPDATE TM_PROJECT" +
            "   SET STATUS = #{status} " +
            " WHERE ID = #{id} AND USER_ID = #{userId}"
    )
    void changeProjectStatusById(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("id") final String id,
            @NotNull @Param("status") final Status status
    );

}
