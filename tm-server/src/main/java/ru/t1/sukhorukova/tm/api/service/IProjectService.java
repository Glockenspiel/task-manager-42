package ru.t1.sukhorukova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.enumerated.ProjectSort;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.dto.model.ProjectDTO;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    @NotNull
    ProjectDTO add(@Nullable ProjectDTO project);

    @Nullable
    ProjectDTO add(
            @Nullable String userId,
            @Nullable ProjectDTO project
    );

    @NotNull
    Collection<ProjectDTO> add(@NotNull Collection<ProjectDTO> projects);

    @NotNull
    Collection<ProjectDTO> set(@NotNull Collection<ProjectDTO> projects);

    @NotNull
    List<ProjectDTO> findAll();

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId);

    @NotNull
    List<ProjectDTO> findAll(
            @Nullable final String userId,
            @Nullable final ProjectSort sort
    );

    @Nullable
    ProjectDTO findOneById(@Nullable String id);

    @Nullable
    ProjectDTO findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    ProjectDTO findOneByIndex(@Nullable Integer index);

    @Nullable
    ProjectDTO findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @NotNull
    ProjectDTO removeOne(@Nullable ProjectDTO project);

    ProjectDTO removeOne(
            @Nullable String userId,
            @Nullable ProjectDTO project
    );

    @Nullable
    ProjectDTO removeOneById(@Nullable String id);

    @Nullable
    ProjectDTO removeOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    ProjectDTO removeOneByIndex(@Nullable Integer index);

    @Nullable
    ProjectDTO removeOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    void removeAll();

    void removeAll(@Nullable String userId);

    int getSize();

    int getSize(@Nullable String userId);

    boolean existsById(String id);

    boolean existsById(
            @Nullable String userId,
            @Nullable String id
    );

    @NotNull
    ProjectDTO create(
            @Nullable final String userId,
            @NotNull String name,
            @NotNull String description
    );

    @NotNull
    ProjectDTO updateById(
            @NotNull final String userId,
            @NotNull String id,
            @NotNull String name,
            @NotNull String description
    );

    @NotNull
    ProjectDTO updateByIndex(
            @NotNull final String userId,
            @NotNull Integer index,
            @NotNull String name,
            @NotNull String description
    );

    @NotNull
    ProjectDTO changeProjectStatusById(
            @NotNull final String userId,
            @NotNull String id,
            @NotNull Status status
    );

    @NotNull
    ProjectDTO changeProjectStatusByIndex(
            @NotNull final String userId,
            @NotNull Integer index,
            @NotNull Status status
    );

}
