package ru.t1.sukhorukova.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.t1.sukhorukova.tm.api.repository.ITaskRepository;
import ru.t1.sukhorukova.tm.api.service.IConnectionService;
import ru.t1.sukhorukova.tm.api.service.IProjectService;
import ru.t1.sukhorukova.tm.api.service.ITaskService;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.enumerated.TaskSort;
import ru.t1.sukhorukova.tm.exception.entity.EntityNotFoundException;
import ru.t1.sukhorukova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.sukhorukova.tm.exception.entity.TaskNotFoundException;
import ru.t1.sukhorukova.tm.exception.entity.ValueIsNullException;
import ru.t1.sukhorukova.tm.exception.field.*;
import ru.t1.sukhorukova.tm.dto.model.TaskDTO;

import java.util.Collection;
import java.util.List;

public final class TaskService implements ITaskService {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    final IProjectService projectService;

    public TaskService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IProjectService projectService
    ) {
        this.connectionService = connectionService;
        this.projectService = projectService;
    }

    @NotNull
    @Override
    public TaskDTO add(@Nullable final TaskDTO task) {
        if (task == null) throw new EntityNotFoundException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            repository.add(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

        return task;
    }

    @Nullable
    @Override
    public TaskDTO add(
            @Nullable final String userId,
            @Nullable final TaskDTO task
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new EntityNotFoundException();

        task.setUserId(userId);

        return add(task);
    }

    @NotNull
    @Override
    public Collection<TaskDTO> add(@Nullable final Collection<TaskDTO> tasks) {
        if (tasks == null) throw new ValueIsNullException();

        for (final TaskDTO task : tasks) {
            add(task);
        }

        return tasks;
    }

    @NotNull
    @Override
    public Collection<TaskDTO> set(@Nullable final Collection<TaskDTO> tasks) {
        if (tasks == null) throw new ValueIsNullException();

        removeAll();

        return add(tasks);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);

        try {
            return repository.findAll();
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            return repository.findAllByUserId(userId);
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(
            @Nullable final String userId,
            @Nullable final TaskSort sort
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            switch (sort) {
                case BY_NAME: return repository.findAllSortByName(userId);
                case BY_STATUS: return repository.findAllSortByStatus(userId);
                case BY_CREATED: return repository.findAllSortByCreated(userId);
                default: return repository.findAll();
            }
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            return repository.findAllByProjectId(userId, projectId);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            return repository.findOneById(id);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            return repository.findOneByIdByUserId(userId, id);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @NotNull final List<TaskDTO> tasks = findAll();
        return tasks.get(index);
    }

    @Nullable
    @Override
    public TaskDTO findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();

        @NotNull final List<TaskDTO> tasks = findAll(userId);
        return tasks.get(index);
    }

    @Override
    public void removeAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            repository.removeAll();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            repository.removeAllByUserId(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO removeOne(@Nullable final TaskDTO task) {
        if (task == null) throw new EntityNotFoundException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            repository.removeOneById(task.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

        return task;
    }

    @Override
    public TaskDTO removeOne(
            @Nullable final String userId,
            @Nullable final TaskDTO task
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new EntityNotFoundException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            repository.removeOneByIdByUserId(userId, task.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

        return task;
    }

    @Nullable
    @Override
    public TaskDTO removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable TaskDTO task = findOneById(id);
        if (task == null) return null;

        return removeOne(task);
    }

    @Nullable
    @Override
    public TaskDTO removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable TaskDTO task = findOneById(userId, id);
        if (task == null) return null;

        return removeOne(userId, task);
    }

    @Nullable
    @Override
    public TaskDTO removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @NotNull final List<TaskDTO> projects = findAll();
        return removeOneById(projects.get(index).getId());
    }

    @Nullable
    @Override
    public TaskDTO removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @NotNull final List<TaskDTO> tasks = findAll(userId);
        return removeOneById(tasks.get(index).getId());
    }

    @Override
    public int getSize() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            return repository.getSize();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            return repository.getSizeByUserId(userId);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return findOneById(id) != null;
    }

    @Override
    public boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return findOneById(userId, id) != null;
    }

    @NotNull
    @Override
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();

        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        task.setStatus(Status.NOT_STARTED);
        add(userId, task);
        return task;
    }

    @NotNull
    @Override
    public TaskDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();

        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new EntityNotFoundException();
        task.setName(name);
        task.setDescription(description);

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            repository.updateById(userId, id, name, description);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

        return task;
    }

    @NotNull
    @Override
    public TaskDTO updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();

        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new EntityNotFoundException();

        return updateById(userId, task.getId(), name, description);
    }

    @NotNull
    @Override
    public TaskDTO changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (status == null) throw new StatusEmptyException();

        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new EntityNotFoundException();
        task.setStatus(status);

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            repository.changeTaskStatusById(userId, id, status);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

        return task;
    }

    @NotNull
    @Override
    public TaskDTO changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();

        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new EntityNotFoundException();

        return changeTaskStatusById(userId, task.getId(), status);
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();

        if (!existsById(taskId)) throw new TaskNotFoundException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            repository.bindTaskToProject(userId, taskId, projectId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();

        if (!existsById(taskId)) throw new TaskNotFoundException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            repository.unbindTaskFromProject(userId, taskId, projectId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            repository.removeTasksByProjectId(userId, projectId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

        projectService.removeOneById(userId, projectId);
    }

}
