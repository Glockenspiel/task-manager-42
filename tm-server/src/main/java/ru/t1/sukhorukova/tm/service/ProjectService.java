package ru.t1.sukhorukova.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.api.repository.IProjectRepository;
import ru.t1.sukhorukova.tm.api.service.IConnectionService;
import ru.t1.sukhorukova.tm.api.service.IProjectService;
import ru.t1.sukhorukova.tm.enumerated.ProjectSort;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.exception.entity.EntityNotFoundException;
import ru.t1.sukhorukova.tm.exception.entity.ValueIsNullException;
import ru.t1.sukhorukova.tm.exception.field.*;
import ru.t1.sukhorukova.tm.dto.model.ProjectDTO;

import java.util.Collection;
import java.util.List;

public final class ProjectService implements IProjectService {

    @NotNull
    protected final IConnectionService connectionService;

    public ProjectService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    public ProjectDTO add(@Nullable final ProjectDTO project) {
        if (project == null) throw new EntityNotFoundException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.add(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

        return project;
    }

    @Nullable
    @Override
    public ProjectDTO add(
            @Nullable final String userId,
            @Nullable final ProjectDTO project
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new EntityNotFoundException();

        project.setUserId(userId);

        return add(project);
    }

    @NotNull
    @Override
    public Collection<ProjectDTO> add(@Nullable final Collection<ProjectDTO> projects) {
        if (projects == null) throw new ValueIsNullException();

        for (final ProjectDTO project : projects) {
            add(project);
        }

        return projects;
    }

    @NotNull
    @Override
    public Collection<ProjectDTO> set(@Nullable final Collection<ProjectDTO> projects) {
        if (projects == null) throw new ValueIsNullException();

        removeAll();

        return add(projects);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);

        try {
            return repository.findAll();
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return repository.findAllByUserId(userId);
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(
            @Nullable final String userId,
            @Nullable final ProjectSort sort
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            switch (sort) {
                case BY_NAME: return repository.findAllSortByName(userId);
                case BY_STATUS: return repository.findAllSortByStatus(userId);
                case BY_CREATED: return repository.findAllSortByCreated(userId);
                default: return repository.findAll();
            }
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return repository.findOneById(id);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return repository.findOneByIdByUserId(userId, id);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @NotNull final List<ProjectDTO> projects = findAll();
        return projects.get(index);
    }

    @Nullable
    @Override
    public ProjectDTO findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();

        @NotNull final List<ProjectDTO> projects = findAll(userId);
        return projects.get(index);
    }

    @Override
    public void removeAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.removeAll();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.removeAllByUserId(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public ProjectDTO removeOne(@Nullable final ProjectDTO project) {
        if (project == null) throw new EntityNotFoundException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.removeOneById(project.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

        return project;
    }

    @Override
    public ProjectDTO removeOne(
            @Nullable final String userId,
            @Nullable final ProjectDTO project
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new EntityNotFoundException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.removeOneByIdByUserId(userId, project.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

        return project;
    }

    @Nullable
    @Override
    public ProjectDTO removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable ProjectDTO project = findOneById(id);
        if (project == null) return null;

        return removeOne(project);
    }

    @Nullable
    @Override
    public ProjectDTO removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable ProjectDTO project = findOneById(userId, id);
        if (project == null) return null;

        return removeOne(userId, project);
    }

    @Nullable
    @Override
    public ProjectDTO removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @NotNull final List<ProjectDTO> projects = findAll();
        return removeOneById(projects.get(index).getId());
    }

    @Nullable
    @Override
    public ProjectDTO removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @NotNull final List<ProjectDTO> projects = findAll(userId);
        return removeOneById(projects.get(index).getId());
    }

    @Override
    public int getSize() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return repository.getSize();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return repository.getSizeByUserId(userId);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return findOneById(id) != null;
    }

    @Override
    public boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return findOneById(userId, id) != null;
    }

    @NotNull
    @Override
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();

        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        project.setStatus(Status.NOT_STARTED);
        add(userId, project);
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();

        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new EntityNotFoundException();
        project.setName(name);
        project.setDescription(description);

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.updateById(userId, id, name, description);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

        return project;
    }

    @NotNull
    @Override
    public ProjectDTO updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();

        @Nullable final ProjectDTO project = findOneByIndex(userId, index);
        if (project == null) throw new EntityNotFoundException();

        return updateById(userId, project.getId(), name, description);
    }

    @NotNull
    @Override
    public ProjectDTO changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (status == null) throw new StatusEmptyException();

        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new EntityNotFoundException();
        project.setStatus(status);

        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.changeProjectStatusById(userId, id, status);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

        return project;
    }

    @NotNull
    @Override
    public ProjectDTO changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();

        @Nullable final ProjectDTO project = findOneByIndex(userId, index);
        if (project == null) throw new EntityNotFoundException();

        return changeProjectStatusById(userId, project.getId(), status);
    }

}
