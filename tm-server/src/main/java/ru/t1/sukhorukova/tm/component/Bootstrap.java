package ru.t1.sukhorukova.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.api.endpoint.*;
import ru.t1.sukhorukova.tm.api.service.*;
import ru.t1.sukhorukova.tm.endpoint.*;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.dto.model.UserDTO;
import ru.t1.sukhorukova.tm.service.*;
import ru.t1.sukhorukova.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements ILocatorService {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(connectionService, projectService);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService, sessionService);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ':' + port + '/' + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void start(@NotNull final String[] args) {
        initPID();
        initLogger();
        initDemoData();
        initBackup();
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() {
        @NotNull final String adminLogin = "admin";
        @NotNull final String user1Login = "USER_01";
        @NotNull final String user2Login = "USER_02";
        @NotNull final String user3Login = "USER_03";

        @NotNull final UserDTO user1;
        @NotNull final UserDTO user2;
        @NotNull final UserDTO user3;

        if (!userService.isLoginExist(adminLogin)) userService.create(adminLogin, "admin", Role.ADMIN);
        if (!userService.isLoginExist(user1Login)) user1 = userService.create(user1Login, "user01");
        else user1 = userService.findByLogin(user1Login);
        if (!userService.isLoginExist(user2Login)) user2 = userService.create(user2Login, "user02", "user02@address.ru");
        else user2 = userService.findByLogin(user2Login);
        if (!userService.isLoginExist(user3Login)) user3 = userService.create(user3Login, "user03", "user03@address.ru");
        else user3 = userService.findByLogin(user3Login);

        projectService.create(user1.getId(), "PROJECT_01", "Test project 1");
        projectService.create(user1.getId(), "PROJECT_18", "Test project 18");
        projectService.create(user2.getId(), "PROJECT_02", "Test project 2");
        projectService.create(user3.getId(), "PROJECT_26", "Test project 26");

        taskService.create(user1.getId(), "TASK_01", "Test task 1");
        taskService.create(user1.getId(), "TASK_18", "Test task 18");
        taskService.create(user1.getId(), "TASK_02", "Test task 2");
        taskService.create(user2.getId(), "TASK_26", "Test task 26");
    }

    private void initBackup() {
        backup.init();
    }

}
