package ru.t1.vsukhorukova.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.dto.model.UserDTO;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@UtilityClass
public final class UserTestData {

    @NotNull
    public final static UserDTO ADMIN = new UserDTO("admin", "admin", Role.ADMIN);

    @NotNull
    public final static UserDTO USER1 = new UserDTO("USER_01", "user01", "user01@address.ru");

    @NotNull
    public final static UserDTO USER2 = new UserDTO("USER_02", "user02", "user02@address.ru");

    @NotNull
    public final static List<UserDTO> USER_LIST = Arrays.asList(ADMIN, USER1, USER2);

    @NotNull
    public final static List<UserDTO> ADMIN_LIST = Collections.singletonList(ADMIN);

    @Nullable
    public final static UserDTO NULL_USER = null;

    @Nullable
    public final static String NULL_USER_ID = null;

    @Nullable
    public final static List<UserDTO> NULL_USER_LIST = null;

}
