package ru.t1.vsukhorukova.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.sukhorukova.tm.api.service.IConnectionService;
import ru.t1.sukhorukova.tm.api.service.IProjectService;
import ru.t1.sukhorukova.tm.api.service.IPropertyService;
import ru.t1.sukhorukova.tm.api.service.IUserService;
import ru.t1.sukhorukova.tm.enumerated.ProjectSort;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.exception.entity.EntityNotFoundException;
import ru.t1.sukhorukova.tm.exception.entity.ValueIsNullException;
import ru.t1.sukhorukova.tm.exception.field.*;
import ru.t1.sukhorukova.tm.dto.model.ProjectDTO;
import ru.t1.sukhorukova.tm.service.ConnectionService;
import ru.t1.sukhorukova.tm.service.ProjectService;
import ru.t1.sukhorukova.tm.service.PropertyService;
import ru.t1.sukhorukova.tm.service.UserService;
import ru.t1.vsukhorukova.tm.marker.UnitCategory;

import java.util.List;

import static ru.t1.vsukhorukova.tm.constant.ProjectTestData.*;
import static ru.t1.vsukhorukova.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.t1.vsukhorukova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @Rule
    public ExpectedException thrown = ExpectedException.none();


    @Before
    public void clear() {
        userService.removeAll();
        userService.add(ADMIN);
        userService.add(USER1);
        userService.add(USER2);
    }

    @Test
    public void testProjectAdd() {
        Assert.assertNotNull(projectService.add(USER1_PROJECT1));
        Assert.assertEquals(USER1_PROJECT1.getId(), projectService.findAll().get(0).getId());

        thrown.expect(EntityNotFoundException.class);
        projectService.add(NULL_PROJECT);
    }

    @Test
    public void testProjectAddByUserId() {
        Assert.assertNotNull(projectService.add(USER1.getId(), USER1_PROJECT1));
        Assert.assertEquals(USER1_PROJECT1.getId(), projectService.findAll().get(0).getId());

        thrown.expect(UserIdEmptyException.class);
        projectService.add(NULL_USER_ID, USER1_PROJECT1);
        thrown.expect(EntityNotFoundException.class);
        projectService.add(USER1.getId(), NULL_PROJECT);
    }

    @Test
    public void testProjectAddModels() {
        Assert.assertNotNull(projectService.add(ADMIN_PROJECT_LIST));
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(0).getId(), projectService.findAll().get(0).getId());
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(1).getId(), projectService.findAll().get(1).getId());

        thrown.expect(ValueIsNullException.class);
        projectService.add(NULL_PROJECT_LIST);
    }

    @Test
    public void testProjectSet() {
        Assert.assertNotNull(projectService.set(ADMIN_PROJECT_LIST));
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(0).getId(), projectService.findAll().get(0).getId());
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(1).getId(), projectService.findAll().get(1).getId());
        Assert.assertNotNull(projectService.set(USER2_PROJECT_LIST));
        Assert.assertEquals(USER2_PROJECT_LIST.get(0).getId(), projectService.findAll().get(0).getId());

        thrown.expect(ValueIsNullException.class);
        projectService.add(NULL_PROJECT_LIST);
    }

    @Test
    public void testProjectFindAll() {
        projectService.add(USER1_PROJECT_LIST);
        @NotNull final List<ProjectDTO> projectList = projectService.findAll();
        Assert.assertEquals(USER1_PROJECT_LIST.get(0).getId(), projectList.get(0).getId());
        Assert.assertEquals(USER1_PROJECT_LIST.get(1).getId(), projectList.get(1).getId());
        Assert.assertEquals(USER1_PROJECT_LIST.get(2).getId(), projectList.get(2).getId());
    }

    @Test
    public void testProjectFindAllByUserId() {
        projectService.add(USER1_PROJECT_LIST);
        projectService.add(ADMIN_PROJECT_LIST);
        @NotNull final List<ProjectDTO> projectListUser1 = projectService.findAll(USER1.getId());
        Assert.assertEquals(USER1_PROJECT_LIST.get(0).getId(), projectListUser1.get(0).getId());
        Assert.assertEquals(USER1_PROJECT_LIST.get(1).getId(), projectListUser1.get(1).getId());
        Assert.assertEquals(USER1_PROJECT_LIST.get(2).getId(), projectListUser1.get(2).getId());
        @NotNull final List<ProjectDTO> projectListAdmin = projectService.findAll(ADMIN.getId());
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(0).getId(), projectListAdmin.get(0).getId());
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(1).getId(), projectListAdmin.get(1).getId());

        thrown.expect(UserIdEmptyException.class);
        projectService.findAll(NULL_USER_ID);
    }

    @Test
    public void testProjectFindAllSortByUserId() {
        projectService.add(ADMIN_PROJECT_LIST.get(1));
        projectService.add(USER1_PROJECT_LIST.get(0));
        projectService.add(ADMIN_PROJECT_LIST.get(0));
        projectService.add(USER1_PROJECT_LIST.get(1));
        @NotNull final ProjectSort sort = ProjectSort.BY_NAME;
        @NotNull final List<ProjectDTO> projectListUser1 = projectService.findAll(USER1.getId(), sort);
        Assert.assertEquals(USER1_PROJECT_LIST.get(0).getId(), projectListUser1.get(0).getId());
        Assert.assertEquals(USER1_PROJECT_LIST.get(1).getId(), projectListUser1.get(1).getId());
        @NotNull final List<ProjectDTO> projectListAdmin = projectService.findAll(ADMIN.getId(), sort);
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(0).getId(), projectListAdmin.get(0).getId());
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(1).getId(), projectListAdmin.get(1).getId());

        thrown.expect(UserIdEmptyException.class);
        projectService.findAll(NULL_USER_ID, sort);
    }

    @Test
    public void testProjectFindOneById() {
        projectService.add(USER1_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT2.getId(), projectService.findOneById(USER1_PROJECT2.getId()).getId());
        Assert.assertNull(projectService.findOneById("test-id"));

        thrown.expect(IdEmptyException.class);
        projectService.findOneById(NULL_PROJECT_ID);
    }

    @Test
    public void testProjectFindOneByIdByUserId() {
        projectService.add(USER1_PROJECT_LIST);
        projectService.add(ADMIN_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT2.getId(), projectService.findOneById(USER1.getId(), USER1_PROJECT2.getId()).getId());
        Assert.assertEquals(ADMIN_PROJECT2.getId(), projectService.findOneById(ADMIN.getId(), ADMIN_PROJECT2.getId()).getId());
        Assert.assertNull(projectService.findOneById(USER1.getId(), ADMIN_PROJECT2.getId()));
        Assert.assertNull(projectService.findOneById(USER1.getId(), "test-id"));

        thrown.expect(UserIdEmptyException.class);
        projectService.findOneById(NULL_USER_ID, ADMIN_PROJECT2.getId());
        thrown.expect(IdEmptyException.class);
        projectService.findOneById(NULL_PROJECT_ID);
    }

    @Test
    public void testProjectFindOneByIndex() {
        projectService.add(USER1_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT2.getId(), projectService.findOneByIndex(1).getId());

        thrown.expect(IndexIncorrectException.class);
        projectService.findOneByIndex(-1);
    }

    @Test
    public void testProjectFindOneByIndexByUserId() {
        projectService.add(USER1_PROJECT_LIST);
        projectService.add(ADMIN_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST.get(1).getId(), projectService.findOneByIndex(USER1.getId(), 1).getId());
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(0).getId(), projectService.findOneByIndex(ADMIN.getId(), 0).getId());

        thrown.expect(UserIdEmptyException.class);
        projectService.findOneByIndex(NULL_USER_ID, 0);
        thrown.expect(IndexIncorrectException.class);
        projectService.findOneByIndex(USER1.getId(), -1);
    }

    @Test
    public void testProjectRemoveOne() {
        projectService.add(USER1_PROJECT_LIST);
        Assert.assertNotNull(projectService.findOneById(USER1_PROJECT2.getId()));
        Assert.assertNotNull(projectService.removeOne(USER1_PROJECT2));
        Assert.assertNull(projectService.findOneById(USER1_PROJECT2.getId()));

        thrown.expect(EntityNotFoundException.class);
        projectService.removeOne(NULL_PROJECT);
    }

    @Test
    public void testProjectRemoveOneByUserId() {
        projectService.add(USER1_PROJECT_LIST);
        projectService.add(USER2_PROJECT_LIST);
        Assert.assertNotNull(projectService.findOneById(USER1_PROJECT2.getId()));
        projectService.removeOne(USER2.getId(), USER1_PROJECT2);
        Assert.assertNotNull(projectService.findOneById(USER1_PROJECT2.getId()));
        projectService.removeOne(USER1.getId(), USER1_PROJECT2);
        Assert.assertNull(projectService.findOneById(USER1_PROJECT2.getId()));

        thrown.expect(UserIdEmptyException.class);
        projectService.removeOne(NULL_USER_ID, USER1_PROJECT2);
        thrown.expect(EntityNotFoundException.class);
        projectService.removeOne(USER1.getId(), NULL_PROJECT);
    }

    @Test
    public void testProjectRemoveOneById() {
        projectService.add(USER1_PROJECT_LIST);
        Assert.assertNotNull(projectService.findOneById(USER1_PROJECT2.getId()));
        Assert.assertNotNull(projectService.removeOneById(USER1_PROJECT2.getId()));
        Assert.assertNull(projectService.findOneById(USER1_PROJECT2.getId()));

        thrown.expect(IdEmptyException.class);
        projectService.removeOneById(NULL_PROJECT_ID);
    }

    @Test
    public void testProjectRemoveOneByIdByUserId() {
        projectService.add(USER1_PROJECT_LIST);
        projectService.add(USER2_PROJECT_LIST);
        Assert.assertNotNull(projectService.findOneById(USER1_PROJECT2.getId()));
        Assert.assertNull(projectService.removeOneById(USER2.getId(), USER1_PROJECT2.getId()));
        Assert.assertNotNull(projectService.findOneById(USER1_PROJECT2.getId()));
        Assert.assertNotNull(projectService.removeOneById(USER1.getId(), USER1_PROJECT2.getId()));
        Assert.assertNull(projectService.findOneById(USER1_PROJECT2.getId()));

        thrown.expect(UserIdEmptyException.class);
        projectService.removeOneById(NULL_USER_ID, USER1_PROJECT2.getId());
        thrown.expect(IdEmptyException.class);
        projectService.removeOneById(USER2.getId(), NULL_PROJECT_ID);
    }

    @Test
    public void testProjectRemoveOneByIndex() {
        projectService.add(USER1_PROJECT1);
        Assert.assertNotNull(projectService.findOneById(USER1_PROJECT1.getId()));
        Assert.assertNotNull(projectService.removeOneByIndex(0));
        Assert.assertNull(projectService.findOneById(USER1_PROJECT1.getId()));

        thrown.expect(IndexIncorrectException.class);
        projectService.removeOneByIndex(-1);
    }

    @Test
    public void testProjectRemoveOneByIndexByUserId() {
        projectService.add(USER1_PROJECT1);
        projectService.add(USER2_PROJECT1);
        Assert.assertNotNull(projectService.findOneById(USER1_PROJECT1.getId()));
        Assert.assertNotNull(projectService.findOneById(USER2_PROJECT1.getId()));
        Assert.assertNotNull(projectService.removeOneByIndex(USER1.getId(), 0));
        Assert.assertNotNull(projectService.findOneById(USER2_PROJECT1.getId()));
        Assert.assertNull(projectService.findOneById(USER1_PROJECT1.getId()));

        thrown.expect(UserIdEmptyException.class);
        projectService.removeOneByIndex(NULL_USER_ID, 0);
        thrown.expect(IndexIncorrectException.class);
        projectService.removeOneByIndex(USER1.getId(), -1);
    }

    @Test
    public void testProjectRemoveAll() {
        projectService.add(USER1_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST.size(), projectService.findAll().size());
        projectService.removeAll();
        Assert.assertEquals(0, projectService.findAll().size());
    }

    @Test
    public void testProjectRemoveAllByUserId() {
        projectService.add(USER1_PROJECT_LIST);
        projectService.add(USER2_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST.size(), projectService.findAll(USER1.getId()).size());
        Assert.assertEquals(USER2_PROJECT_LIST.size(), projectService.findAll(USER2.getId()).size());
        projectService.removeAll(USER1.getId());
        Assert.assertEquals(0, projectService.findAll(USER1.getId()).size());
        Assert.assertEquals(USER2_PROJECT_LIST.size(), projectService.findAll(USER2.getId()).size());

        thrown.expect(UserIdEmptyException.class);
        projectService.removeAll(NULL_USER_ID);
    }

    @Test
    public void testProjectGetSize() {
        projectService.add(USER1_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST.size(), projectService.getSize());
    }

    @Test
    public void testProjectGetSizeByUserId() {
        projectService.add(USER1_PROJECT_LIST);
        projectService.add(USER2_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST.size(), projectService.getSize(USER1.getId()));
        Assert.assertEquals(USER2_PROJECT_LIST.size(), projectService.getSize(USER2.getId()));

        thrown.expect(UserIdEmptyException.class);
        projectService.getSize(NULL_USER_ID);
    }

    @Test
    public void testProjectExistsById() {
        projectService.add(USER1_PROJECT_LIST);
        Assert.assertTrue(projectService.existsById(USER1_PROJECT1.getId()));
        Assert.assertFalse(projectService.existsById(USER2_PROJECT1.getId()));
    }

    @Test
    public void testProjectExistsByIdByUserId() {
        projectService.add(USER1_PROJECT_LIST);
        projectService.add(USER2_PROJECT_LIST);
        Assert.assertTrue(projectService.existsById(USER1.getId(), USER1_PROJECT1.getId()));
        Assert.assertFalse(projectService.existsById(USER2.getId(), USER1_PROJECT1.getId()));

        thrown.expect(UserIdEmptyException.class);
        projectService.existsById(NULL_USER_ID, USER1_PROJECT1.getId());
    }

    @Test
    public void testProjectCreate() {
        Assert.assertEquals(0, projectService.findAll(USER1.getId()).size());
        @NotNull final String name = "PROJECT_TEST";
        @NotNull final String description = "Description";
        Assert.assertNotNull(projectService.create(USER1.getId(), name, description));
        Assert.assertEquals(1, projectService.findAll(USER1.getId()).size());
        @NotNull final ProjectDTO project = projectService.findOneByIndex(0);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER1.getId(), project.getUserId());
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());

        thrown.expect(UserIdEmptyException.class);
        projectService.create(NULL_USER_ID, name, description);
        thrown.expect(NameEmptyException.class);
        projectService.create(USER1.getId(), null, description);
        thrown.expect(DescriptionEmptyException.class);
        projectService.create(USER1.getId(), name, null);
    }

    @Test
    public void testProjectUpdateById() {
        projectService.add(USER1_PROJECT_LIST);
        @NotNull final String name = "PROJECT_TEST";
        @NotNull final String description = "Description";
        Assert.assertNotNull(projectService.updateById(USER1.getId(), USER1_PROJECT2.getId(), name, description));
        @NotNull final ProjectDTO project = projectService.findOneById(USER1_PROJECT2.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());

        thrown.expect(UserIdEmptyException.class);
        projectService.updateById(NULL_USER_ID, USER1_PROJECT2.getId(), name, description);
        thrown.expect(ProjectIdEmptyException.class);
        projectService.updateById(USER1.getId(), NULL_PROJECT_ID, name, description);
        thrown.expect(NameEmptyException.class);
        projectService.updateById(USER1.getId(), USER1_PROJECT2.getId(), null, description);
        thrown.expect(DescriptionEmptyException.class);
        projectService.updateById(USER1.getId(), USER1_PROJECT2.getId(), name, null);
    }

    @Test
    public void testProjectUpdateByIndex() {
        projectService.add(USER1_PROJECT_LIST);
        @NotNull final String name = "PROJECT_TEST";
        @NotNull final String description = "Description";
        Assert.assertNotNull(projectService.updateByIndex(USER1.getId(), 1, name, description));
        @NotNull final ProjectDTO project = projectService.findOneById(USER1_PROJECT2.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());

        thrown.expect(UserIdEmptyException.class);
        projectService.updateByIndex(NULL_USER_ID, 1, name, description);
        thrown.expect(IndexIncorrectException.class);
        projectService.updateByIndex(USER1.getId(), -1, name, description);
        thrown.expect(NameEmptyException.class);
        projectService.updateByIndex(USER1.getId(), 1, null, description);
        thrown.expect(DescriptionEmptyException.class);
        projectService.updateByIndex(USER1.getId(), 1, name, null);
    }

    @Test
    public void testProjectChangeProjectStatusById() {
        projectService.add(USER1_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT1.getStatus(), projectService.findOneById(USER1_PROJECT1.getId()).getStatus());
        Assert.assertNotNull(projectService.changeProjectStatusById(USER1.getId(), USER1_PROJECT1.getId(), Status.NOT_STARTED));
        Assert.assertEquals(Status.NOT_STARTED, projectService.findOneById(USER1_PROJECT1.getId()).getStatus());

        thrown.expect(UserIdEmptyException.class);
        projectService.changeProjectStatusById(NULL_USER_ID, USER1_PROJECT1.getId(), Status.NOT_STARTED);
        thrown.expect(ProjectIdEmptyException.class);
        projectService.changeProjectStatusById(USER1.getId(), NULL_PROJECT_ID, Status.NOT_STARTED);
        thrown.expect(StatusEmptyException.class);
        projectService.changeProjectStatusById(USER1.getId(), USER1_PROJECT1.getId(), null);
    }

    @Test
    public void testProjectChangeProjectStatusByIndex() {
        projectService.add(USER1_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST.get(0).getStatus(), projectService.findOneByIndex(0).getStatus());
        Assert.assertNotNull(projectService.changeProjectStatusByIndex(USER1.getId(), 0, Status.NOT_STARTED));
        Assert.assertEquals(Status.NOT_STARTED, projectService.findOneById(USER1_PROJECT1.getId()).getStatus());

        thrown.expect(UserIdEmptyException.class);
        projectService.changeProjectStatusByIndex(NULL_USER_ID, 0, Status.NOT_STARTED);
        thrown.expect(IndexIncorrectException.class);
        projectService.changeProjectStatusByIndex(USER1.getId(), -1, Status.NOT_STARTED);
        thrown.expect(StatusEmptyException.class);
        projectService.changeProjectStatusByIndex(USER1.getId(), 0, null);
    }

}
